package com.camoesradio.mobile1.bean.response;

import java.util.List;

/**
 * Created by sumit on 21/3/17.
 */

public class GetApiResponse {


    /**
     * env : production
     * schedulerTime : 2017-03-21 06:18:34
     * previous : {"starts":"2017-03-21 10:10:52.000000","ends":"2017-03-21 10:14:37.000000","type":"track","name":"Stephanie Tavares - Sinal do Coracao","metadata":{"id":37263,"name":"","mime":"audio/mp3","ftype":"audioclip","directory":null,"filepath":"578e31c828923-Sinal do Coracao.mp3","import_status":0,"currentlyaccessing":0,"editedby":null,"mtime":"2016-07-19 13:57:33","utime":"2016-07-19 13:57:28","lptime":"2017-03-21 10:10:52","md5":"3724fd2791385a003785191d0e401da7","track_title":"Sinal do Coracao","artist_name":"Stephanie Tavares","bit_rate":320000,"sample_rate":44100,"format":null,"length":"00:03:49.2134","album_title":"Sinal do Coração","genre":null,"comments":"","year":"2014","track_number":3,"channels":2,"url":null,"bpm":null,"rating":null,"encoded_by":null,"disc_number":null,"mood":null,"label":null,"composer":null,"encoder":null,"checksum":null,"lyrics":null,"orchestra":null,"conductor":null,"lyricist":null,"original_lyricist":null,"radio_station_name":null,"info_url":null,"artist_url":null,"audio_source_url":null,"radio_station_url":null,"buy_this_url":null,"isrc_number":null,"catalog_number":null,"original_artist":null,"copyright":null,"report_datetime":null,"report_location":null,"report_organization":null,"subject":null,"contributor":null,"language":null,"soundcloud_id":null,"soundcloud_error_code":null,"soundcloud_error_msg":null,"soundcloud_link_to_file":null,"soundcloud_upload_time":null,"replay_gain":"-9.5","owner_id":1,"cuein":"00:00:00.266236","cueout":"00:03:46.254331","hidden":false,"filesize":9172888,"description":null,"cloud_file_id":17804}}
     * current : {"starts":"2017-03-21 10:14:35","ends":"2017-03-21 10:18:49","type":"track","name":"Henrik Cipriano - As Velhas Do Milenio","media_item_played":true,"metadata":{"id":37351,"name":"","mime":"audio/mp3","ftype":"audioclip","directory":null,"filepath":"09 As Velhas Do Milenio.mp3","import_status":0,"currentlyaccessing":0,"editedby":null,"mtime":"2016-07-20 19:28:17","utime":"2016-07-20 19:28:13","lptime":"2017-03-21 10:14:35","md5":"114e5246c37e07113b7cd77089d1f1bb","track_title":"As Velhas Do Milenio","artist_name":"Henrik Cipriano","bit_rate":320000,"sample_rate":44100,"format":null,"length":"00:04:13.83185","album_title":"Recordações","genre":"Portugese","comments":"","year":"2002","track_number":9,"channels":2,"url":null,"bpm":null,"rating":null,"encoded_by":null,"disc_number":null,"mood":null,"label":null,"composer":null,"encoder":null,"checksum":null,"lyrics":null,"orchestra":null,"conductor":null,"lyricist":null,"original_lyricist":null,"radio_station_name":null,"info_url":null,"artist_url":null,"audio_source_url":null,"radio_station_url":null,"buy_this_url":null,"isrc_number":null,"catalog_number":null,"original_artist":null,"copyright":null,"report_datetime":null,"report_location":null,"report_organization":null,"subject":null,"contributor":null,"language":null,"soundcloud_id":null,"soundcloud_error_code":null,"soundcloud_error_msg":null,"soundcloud_link_to_file":null,"soundcloud_upload_time":null,"replay_gain":"-5.45","owner_id":1,"cuein":"00:00:00","cueout":"00:04:13.83185","hidden":false,"filesize":11368982,"description":null,"cloud_file_id":17892},"record":"0"}
     * next : {"starts":"2017-03-21 10:18:47.000000","ends":"2017-03-21 10:24:42.000000","type":"track","name":"Jose Cid - Verdes trigais em flor","metadata":{"id":37628,"name":"","mime":"audio/mp3","ftype":"audioclip","directory":null,"filepath":"13 Verdes trigais em flor.mp3","import_status":0,"currentlyaccessing":0,"editedby":null,"mtime":"2016-07-27 00:18:23","utime":"2016-07-27 00:18:18","lptime":"2017-03-21 08:25:57","md5":"0943cf66f9096734180e7740f6185a52","track_title":"Verdes trigais em flor","artist_name":"Jose Cid","bit_rate":256000,"sample_rate":44100,"format":null,"length":"00:05:59.941188","album_title":"O melhor dos melhores","genre":"Portuguesa","comments":"","year":null,"track_number":13,"channels":2,"url":null,"bpm":null,"rating":null,"encoded_by":null,"disc_number":null,"mood":null,"label":null,"composer":null,"encoder":null,"checksum":null,"lyrics":null,"orchestra":null,"conductor":null,"lyricist":null,"original_lyricist":null,"radio_station_name":null,"info_url":null,"artist_url":null,"audio_source_url":null,"radio_station_url":null,"buy_this_url":null,"isrc_number":null,"catalog_number":null,"original_artist":null,"copyright":null,"report_datetime":null,"report_location":null,"report_organization":null,"subject":null,"contributor":null,"language":null,"soundcloud_id":null,"soundcloud_error_code":null,"soundcloud_error_msg":null,"soundcloud_link_to_file":null,"soundcloud_upload_time":null,"replay_gain":"-1.27","owner_id":1,"cuein":"00:00:00.996531","cueout":"00:05:56.482404","hidden":false,"filesize":11522470,"description":null,"cloud_file_id":18169}}
     * currentShow : [{"start_timestamp":"2017-03-21 06:00:00","end_timestamp":"2017-03-21 07:00:00","name":"Musica Variada","description":"","id":3155,"instance_id":6512,"record":0,"url":"","image_path":"","image_cloud_file_id":null,"auto_dj":true,"starts":"2017-03-21 06:00:00","ends":"2017-03-21 07:00:00"}]
     * nextShow : [{"id":2099,"instance_id":5832,"name":"CamoesRadioCKWR 5 to 8 pm (Repetiçao)","description":"","url":"www.camoesradio.com","start_timestamp":"2017-03-21 07:00:00","end_timestamp":"2017-03-21 10:00:00","starts":"2017-03-21 07:00:00","ends":"2017-03-21 10:00:00","record":0,"image_path":"","image_cloud_file_id":null,"auto_dj":false,"type":"show"}]
     * source_enabled : Scheduled
     * timezone : America/Toronto
     * timezoneOffset : -14400
     * AIRTIME_API_VERSION : 1.1
     */

    private String env;
    private String schedulerTime;
    private PreviousBean previous;
    private CurrentBean current;
    private NextBean next;
    private String source_enabled;
    private String timezone;
    private String timezoneOffset;
    private String AIRTIME_API_VERSION;
    private List<CurrentShowBean> currentShow;
    private List<NextShowBean> nextShow;

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getSchedulerTime() {
        return schedulerTime;
    }

    public void setSchedulerTime(String schedulerTime) {
        this.schedulerTime = schedulerTime;
    }

    public PreviousBean getPrevious() {
        return previous;
    }

    public void setPrevious(PreviousBean previous) {
        this.previous = previous;
    }

    public CurrentBean getCurrent() {
        return current;
    }

    public void setCurrent(CurrentBean current) {
        this.current = current;
    }

    public NextBean getNext() {
        return next;
    }

    public void setNext(NextBean next) {
        this.next = next;
    }

    public String getSource_enabled() {
        return source_enabled;
    }

    public void setSource_enabled(String source_enabled) {
        this.source_enabled = source_enabled;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(String timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public String getAIRTIME_API_VERSION() {
        return AIRTIME_API_VERSION;
    }

    public void setAIRTIME_API_VERSION(String AIRTIME_API_VERSION) {
        this.AIRTIME_API_VERSION = AIRTIME_API_VERSION;
    }

    public List<CurrentShowBean> getCurrentShow() {
        return currentShow;
    }

    public void setCurrentShow(List<CurrentShowBean> currentShow) {
        this.currentShow = currentShow;
    }

    public List<NextShowBean> getNextShow() {
        return nextShow;
    }

    public void setNextShow(List<NextShowBean> nextShow) {
        this.nextShow = nextShow;
    }

    public static class PreviousBean {
        /**
         * starts : 2017-03-21 10:10:52.000000
         * ends : 2017-03-21 10:14:37.000000
         * type : track
         * name : Stephanie Tavares - Sinal do Coracao
         * metadata : {"id":37263,"name":"","mime":"audio/mp3","ftype":"audioclip","directory":null,"filepath":"578e31c828923-Sinal do Coracao.mp3","import_status":0,"currentlyaccessing":0,"editedby":null,"mtime":"2016-07-19 13:57:33","utime":"2016-07-19 13:57:28","lptime":"2017-03-21 10:10:52","md5":"3724fd2791385a003785191d0e401da7","track_title":"Sinal do Coracao","artist_name":"Stephanie Tavares","bit_rate":320000,"sample_rate":44100,"format":null,"length":"00:03:49.2134","album_title":"Sinal do Coração","genre":null,"comments":"","year":"2014","track_number":3,"channels":2,"url":null,"bpm":null,"rating":null,"encoded_by":null,"disc_number":null,"mood":null,"label":null,"composer":null,"encoder":null,"checksum":null,"lyrics":null,"orchestra":null,"conductor":null,"lyricist":null,"original_lyricist":null,"radio_station_name":null,"info_url":null,"artist_url":null,"audio_source_url":null,"radio_station_url":null,"buy_this_url":null,"isrc_number":null,"catalog_number":null,"original_artist":null,"copyright":null,"report_datetime":null,"report_location":null,"report_organization":null,"subject":null,"contributor":null,"language":null,"soundcloud_id":null,"soundcloud_error_code":null,"soundcloud_error_msg":null,"soundcloud_link_to_file":null,"soundcloud_upload_time":null,"replay_gain":"-9.5","owner_id":1,"cuein":"00:00:00.266236","cueout":"00:03:46.254331","hidden":false,"filesize":9172888,"description":null,"cloud_file_id":17804}
         */

        private String starts;
        private String ends;
        private String type;
        private String name;
        private MetadataBean metadata;

        public String getStarts() {
            return starts;
        }

        public void setStarts(String starts) {
            this.starts = starts;
        }

        public String getEnds() {
            return ends;
        }

        public void setEnds(String ends) {
            this.ends = ends;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public MetadataBean getMetadata() {
            return metadata;
        }

        public void setMetadata(MetadataBean metadata) {
            this.metadata = metadata;
        }

        public static class MetadataBean {
            /**
             * id : 37263
             * name :
             * mime : audio/mp3
             * ftype : audioclip
             * directory : null
             * filepath : 578e31c828923-Sinal do Coracao.mp3
             * import_status : 0
             * currentlyaccessing : 0
             * editedby : null
             * mtime : 2016-07-19 13:57:33
             * utime : 2016-07-19 13:57:28
             * lptime : 2017-03-21 10:10:52
             * md5 : 3724fd2791385a003785191d0e401da7
             * track_title : Sinal do Coracao
             * artist_name : Stephanie Tavares
             * bit_rate : 320000
             * sample_rate : 44100
             * format : null
             * length : 00:03:49.2134
             * album_title : Sinal do Coração
             * genre : null
             * comments :
             * year : 2014
             * track_number : 3
             * channels : 2
             * url : null
             * bpm : null
             * rating : null
             * encoded_by : null
             * disc_number : null
             * mood : null
             * label : null
             * composer : null
             * encoder : null
             * checksum : null
             * lyrics : null
             * orchestra : null
             * conductor : null
             * lyricist : null
             * original_lyricist : null
             * radio_station_name : null
             * info_url : null
             * artist_url : null
             * audio_source_url : null
             * radio_station_url : null
             * buy_this_url : null
             * isrc_number : null
             * catalog_number : null
             * original_artist : null
             * copyright : null
             * report_datetime : null
             * report_location : null
             * report_organization : null
             * subject : null
             * contributor : null
             * language : null
             * soundcloud_id : null
             * soundcloud_error_code : null
             * soundcloud_error_msg : null
             * soundcloud_link_to_file : null
             * soundcloud_upload_time : null
             * replay_gain : -9.5
             * owner_id : 1
             * cuein : 00:00:00.266236
             * cueout : 00:03:46.254331
             * hidden : false
             * filesize : 9172888
             * description : null
             * cloud_file_id : 17804
             */

            private int id;
            private String name;
            private String mime;
            private String ftype;
            private Object directory;
            private String filepath;
            private int import_status;
            private int currentlyaccessing;
            private Object editedby;
            private String mtime;
            private String utime;
            private String lptime;
            private String md5;
            private String track_title;
            private String artist_name;
            private int bit_rate;
            private int sample_rate;
            private Object format;
            private String length;
            private String album_title;
            private Object genre;
            private String comments;
            private String year;
            private int track_number;
            private int channels;
            private Object url;
            private Object bpm;
            private Object rating;
            private Object encoded_by;
            private Object disc_number;
            private Object mood;
            private Object label;
            private Object composer;
            private Object encoder;
            private Object checksum;
            private Object lyrics;
            private Object orchestra;
            private Object conductor;
            private Object lyricist;
            private Object original_lyricist;
            private Object radio_station_name;
            private Object info_url;
            private Object artist_url;
            private Object audio_source_url;
            private Object radio_station_url;
            private Object buy_this_url;
            private Object isrc_number;
            private Object catalog_number;
            private Object original_artist;
            private Object copyright;
            private Object report_datetime;
            private Object report_location;
            private Object report_organization;
            private Object subject;
            private Object contributor;
            private Object language;
            private Object soundcloud_id;
            private Object soundcloud_error_code;
            private Object soundcloud_error_msg;
            private Object soundcloud_link_to_file;
            private Object soundcloud_upload_time;
            private String replay_gain;
            private int owner_id;
            private String cuein;
            private String cueout;
            private boolean hidden;
            private int filesize;
            private Object description;
            private int cloud_file_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMime() {
                return mime;
            }

            public void setMime(String mime) {
                this.mime = mime;
            }

            public String getFtype() {
                return ftype;
            }

            public void setFtype(String ftype) {
                this.ftype = ftype;
            }

            public Object getDirectory() {
                return directory;
            }

            public void setDirectory(Object directory) {
                this.directory = directory;
            }

            public String getFilepath() {
                return filepath;
            }

            public void setFilepath(String filepath) {
                this.filepath = filepath;
            }

            public int getImport_status() {
                return import_status;
            }

            public void setImport_status(int import_status) {
                this.import_status = import_status;
            }

            public int getCurrentlyaccessing() {
                return currentlyaccessing;
            }

            public void setCurrentlyaccessing(int currentlyaccessing) {
                this.currentlyaccessing = currentlyaccessing;
            }

            public Object getEditedby() {
                return editedby;
            }

            public void setEditedby(Object editedby) {
                this.editedby = editedby;
            }

            public String getMtime() {
                return mtime;
            }

            public void setMtime(String mtime) {
                this.mtime = mtime;
            }

            public String getUtime() {
                return utime;
            }

            public void setUtime(String utime) {
                this.utime = utime;
            }

            public String getLptime() {
                return lptime;
            }

            public void setLptime(String lptime) {
                this.lptime = lptime;
            }

            public String getMd5() {
                return md5;
            }

            public void setMd5(String md5) {
                this.md5 = md5;
            }

            public String getTrack_title() {
                return track_title;
            }

            public void setTrack_title(String track_title) {
                this.track_title = track_title;
            }

            public String getArtist_name() {
                return artist_name;
            }

            public void setArtist_name(String artist_name) {
                this.artist_name = artist_name;
            }

            public int getBit_rate() {
                return bit_rate;
            }

            public void setBit_rate(int bit_rate) {
                this.bit_rate = bit_rate;
            }

            public int getSample_rate() {
                return sample_rate;
            }

            public void setSample_rate(int sample_rate) {
                this.sample_rate = sample_rate;
            }

            public Object getFormat() {
                return format;
            }

            public void setFormat(Object format) {
                this.format = format;
            }

            public String getLength() {
                return length;
            }

            public void setLength(String length) {
                this.length = length;
            }

            public String getAlbum_title() {
                return album_title;
            }

            public void setAlbum_title(String album_title) {
                this.album_title = album_title;
            }

            public Object getGenre() {
                return genre;
            }

            public void setGenre(Object genre) {
                this.genre = genre;
            }

            public String getComments() {
                return comments;
            }

            public void setComments(String comments) {
                this.comments = comments;
            }

            public String getYear() {
                return year;
            }

            public void setYear(String year) {
                this.year = year;
            }

            public int getTrack_number() {
                return track_number;
            }

            public void setTrack_number(int track_number) {
                this.track_number = track_number;
            }

            public int getChannels() {
                return channels;
            }

            public void setChannels(int channels) {
                this.channels = channels;
            }

            public Object getUrl() {
                return url;
            }

            public void setUrl(Object url) {
                this.url = url;
            }

            public Object getBpm() {
                return bpm;
            }

            public void setBpm(Object bpm) {
                this.bpm = bpm;
            }

            public Object getRating() {
                return rating;
            }

            public void setRating(Object rating) {
                this.rating = rating;
            }

            public Object getEncoded_by() {
                return encoded_by;
            }

            public void setEncoded_by(Object encoded_by) {
                this.encoded_by = encoded_by;
            }

            public Object getDisc_number() {
                return disc_number;
            }

            public void setDisc_number(Object disc_number) {
                this.disc_number = disc_number;
            }

            public Object getMood() {
                return mood;
            }

            public void setMood(Object mood) {
                this.mood = mood;
            }

            public Object getLabel() {
                return label;
            }

            public void setLabel(Object label) {
                this.label = label;
            }

            public Object getComposer() {
                return composer;
            }

            public void setComposer(Object composer) {
                this.composer = composer;
            }

            public Object getEncoder() {
                return encoder;
            }

            public void setEncoder(Object encoder) {
                this.encoder = encoder;
            }

            public Object getChecksum() {
                return checksum;
            }

            public void setChecksum(Object checksum) {
                this.checksum = checksum;
            }

            public Object getLyrics() {
                return lyrics;
            }

            public void setLyrics(Object lyrics) {
                this.lyrics = lyrics;
            }

            public Object getOrchestra() {
                return orchestra;
            }

            public void setOrchestra(Object orchestra) {
                this.orchestra = orchestra;
            }

            public Object getConductor() {
                return conductor;
            }

            public void setConductor(Object conductor) {
                this.conductor = conductor;
            }

            public Object getLyricist() {
                return lyricist;
            }

            public void setLyricist(Object lyricist) {
                this.lyricist = lyricist;
            }

            public Object getOriginal_lyricist() {
                return original_lyricist;
            }

            public void setOriginal_lyricist(Object original_lyricist) {
                this.original_lyricist = original_lyricist;
            }

            public Object getRadio_station_name() {
                return radio_station_name;
            }

            public void setRadio_station_name(Object radio_station_name) {
                this.radio_station_name = radio_station_name;
            }

            public Object getInfo_url() {
                return info_url;
            }

            public void setInfo_url(Object info_url) {
                this.info_url = info_url;
            }

            public Object getArtist_url() {
                return artist_url;
            }

            public void setArtist_url(Object artist_url) {
                this.artist_url = artist_url;
            }

            public Object getAudio_source_url() {
                return audio_source_url;
            }

            public void setAudio_source_url(Object audio_source_url) {
                this.audio_source_url = audio_source_url;
            }

            public Object getRadio_station_url() {
                return radio_station_url;
            }

            public void setRadio_station_url(Object radio_station_url) {
                this.radio_station_url = radio_station_url;
            }

            public Object getBuy_this_url() {
                return buy_this_url;
            }

            public void setBuy_this_url(Object buy_this_url) {
                this.buy_this_url = buy_this_url;
            }

            public Object getIsrc_number() {
                return isrc_number;
            }

            public void setIsrc_number(Object isrc_number) {
                this.isrc_number = isrc_number;
            }

            public Object getCatalog_number() {
                return catalog_number;
            }

            public void setCatalog_number(Object catalog_number) {
                this.catalog_number = catalog_number;
            }

            public Object getOriginal_artist() {
                return original_artist;
            }

            public void setOriginal_artist(Object original_artist) {
                this.original_artist = original_artist;
            }

            public Object getCopyright() {
                return copyright;
            }

            public void setCopyright(Object copyright) {
                this.copyright = copyright;
            }

            public Object getReport_datetime() {
                return report_datetime;
            }

            public void setReport_datetime(Object report_datetime) {
                this.report_datetime = report_datetime;
            }

            public Object getReport_location() {
                return report_location;
            }

            public void setReport_location(Object report_location) {
                this.report_location = report_location;
            }

            public Object getReport_organization() {
                return report_organization;
            }

            public void setReport_organization(Object report_organization) {
                this.report_organization = report_organization;
            }

            public Object getSubject() {
                return subject;
            }

            public void setSubject(Object subject) {
                this.subject = subject;
            }

            public Object getContributor() {
                return contributor;
            }

            public void setContributor(Object contributor) {
                this.contributor = contributor;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public Object getSoundcloud_id() {
                return soundcloud_id;
            }

            public void setSoundcloud_id(Object soundcloud_id) {
                this.soundcloud_id = soundcloud_id;
            }

            public Object getSoundcloud_error_code() {
                return soundcloud_error_code;
            }

            public void setSoundcloud_error_code(Object soundcloud_error_code) {
                this.soundcloud_error_code = soundcloud_error_code;
            }

            public Object getSoundcloud_error_msg() {
                return soundcloud_error_msg;
            }

            public void setSoundcloud_error_msg(Object soundcloud_error_msg) {
                this.soundcloud_error_msg = soundcloud_error_msg;
            }

            public Object getSoundcloud_link_to_file() {
                return soundcloud_link_to_file;
            }

            public void setSoundcloud_link_to_file(Object soundcloud_link_to_file) {
                this.soundcloud_link_to_file = soundcloud_link_to_file;
            }

            public Object getSoundcloud_upload_time() {
                return soundcloud_upload_time;
            }

            public void setSoundcloud_upload_time(Object soundcloud_upload_time) {
                this.soundcloud_upload_time = soundcloud_upload_time;
            }

            public String getReplay_gain() {
                return replay_gain;
            }

            public void setReplay_gain(String replay_gain) {
                this.replay_gain = replay_gain;
            }

            public int getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(int owner_id) {
                this.owner_id = owner_id;
            }

            public String getCuein() {
                return cuein;
            }

            public void setCuein(String cuein) {
                this.cuein = cuein;
            }

            public String getCueout() {
                return cueout;
            }

            public void setCueout(String cueout) {
                this.cueout = cueout;
            }

            public boolean isHidden() {
                return hidden;
            }

            public void setHidden(boolean hidden) {
                this.hidden = hidden;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public Object getDescription() {
                return description;
            }

            public void setDescription(Object description) {
                this.description = description;
            }

            public int getCloud_file_id() {
                return cloud_file_id;
            }

            public void setCloud_file_id(int cloud_file_id) {
                this.cloud_file_id = cloud_file_id;
            }
        }
    }

    public static class CurrentBean {
        /**
         * starts : 2017-03-21 10:14:35
         * ends : 2017-03-21 10:18:49
         * type : track
         * name : Henrik Cipriano - As Velhas Do Milenio
         * media_item_played : true
         * metadata : {"id":37351,"name":"","mime":"audio/mp3","ftype":"audioclip","directory":null,"filepath":"09 As Velhas Do Milenio.mp3","import_status":0,"currentlyaccessing":0,"editedby":null,"mtime":"2016-07-20 19:28:17","utime":"2016-07-20 19:28:13","lptime":"2017-03-21 10:14:35","md5":"114e5246c37e07113b7cd77089d1f1bb","track_title":"As Velhas Do Milenio","artist_name":"Henrik Cipriano","bit_rate":320000,"sample_rate":44100,"format":null,"length":"00:04:13.83185","album_title":"Recordações","genre":"Portugese","comments":"","year":"2002","track_number":9,"channels":2,"url":null,"bpm":null,"rating":null,"encoded_by":null,"disc_number":null,"mood":null,"label":null,"composer":null,"encoder":null,"checksum":null,"lyrics":null,"orchestra":null,"conductor":null,"lyricist":null,"original_lyricist":null,"radio_station_name":null,"info_url":null,"artist_url":null,"audio_source_url":null,"radio_station_url":null,"buy_this_url":null,"isrc_number":null,"catalog_number":null,"original_artist":null,"copyright":null,"report_datetime":null,"report_location":null,"report_organization":null,"subject":null,"contributor":null,"language":null,"soundcloud_id":null,"soundcloud_error_code":null,"soundcloud_error_msg":null,"soundcloud_link_to_file":null,"soundcloud_upload_time":null,"replay_gain":"-5.45","owner_id":1,"cuein":"00:00:00","cueout":"00:04:13.83185","hidden":false,"filesize":11368982,"description":null,"cloud_file_id":17892}
         * record : 0
         */

        private String starts;
        private String ends;
        private String type;
        private String name;
        private boolean media_item_played;
        private MetadataBeanX metadata;
        private String record;

        public String getStarts() {
            return starts;
        }

        public void setStarts(String starts) {
            this.starts = starts;
        }

        public String getEnds() {
            return ends;
        }

        public void setEnds(String ends) {
            this.ends = ends;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isMedia_item_played() {
            return media_item_played;
        }

        public void setMedia_item_played(boolean media_item_played) {
            this.media_item_played = media_item_played;
        }

        public MetadataBeanX getMetadata() {
            return metadata;
        }

        public void setMetadata(MetadataBeanX metadata) {
            this.metadata = metadata;
        }

        public String getRecord() {
            return record;
        }

        public void setRecord(String record) {
            this.record = record;
        }

        public static class MetadataBeanX {
            /**
             * id : 37351
             * name :
             * mime : audio/mp3
             * ftype : audioclip
             * directory : null
             * filepath : 09 As Velhas Do Milenio.mp3
             * import_status : 0
             * currentlyaccessing : 0
             * editedby : null
             * mtime : 2016-07-20 19:28:17
             * utime : 2016-07-20 19:28:13
             * lptime : 2017-03-21 10:14:35
             * md5 : 114e5246c37e07113b7cd77089d1f1bb
             * track_title : As Velhas Do Milenio
             * artist_name : Henrik Cipriano
             * bit_rate : 320000
             * sample_rate : 44100
             * format : null
             * length : 00:04:13.83185
             * album_title : Recordações
             * genre : Portugese
             * comments :
             * year : 2002
             * track_number : 9
             * channels : 2
             * url : null
             * bpm : null
             * rating : null
             * encoded_by : null
             * disc_number : null
             * mood : null
             * label : null
             * composer : null
             * encoder : null
             * checksum : null
             * lyrics : null
             * orchestra : null
             * conductor : null
             * lyricist : null
             * original_lyricist : null
             * radio_station_name : null
             * info_url : null
             * artist_url : null
             * audio_source_url : null
             * radio_station_url : null
             * buy_this_url : null
             * isrc_number : null
             * catalog_number : null
             * original_artist : null
             * copyright : null
             * report_datetime : null
             * report_location : null
             * report_organization : null
             * subject : null
             * contributor : null
             * language : null
             * soundcloud_id : null
             * soundcloud_error_code : null
             * soundcloud_error_msg : null
             * soundcloud_link_to_file : null
             * soundcloud_upload_time : null
             * replay_gain : -5.45
             * owner_id : 1
             * cuein : 00:00:00
             * cueout : 00:04:13.83185
             * hidden : false
             * filesize : 11368982
             * description : null
             * cloud_file_id : 17892
             */

            private int id;
            private String name;
            private String mime;
            private String ftype;
            private Object directory;
            private String filepath;
            private int import_status;
            private int currentlyaccessing;
            private Object editedby;
            private String mtime;
            private String utime;
            private String lptime;
            private String md5;
            private String track_title;
            private String artist_name;
            private int bit_rate;
            private int sample_rate;
            private Object format;
            private String length;
            private String album_title;
            private String genre;
            private String comments;
            private String year;
            private int track_number;
            private int channels;
            private Object url;
            private Object bpm;
            private Object rating;
            private Object encoded_by;
            private Object disc_number;
            private Object mood;
            private Object label;
            private Object composer;
            private Object encoder;
            private Object checksum;
            private Object lyrics;
            private Object orchestra;
            private Object conductor;
            private Object lyricist;
            private Object original_lyricist;
            private Object radio_station_name;
            private Object info_url;
            private Object artist_url;
            private Object audio_source_url;
            private Object radio_station_url;
            private Object buy_this_url;
            private Object isrc_number;
            private Object catalog_number;
            private Object original_artist;
            private Object copyright;
            private Object report_datetime;
            private Object report_location;
            private Object report_organization;
            private Object subject;
            private Object contributor;
            private Object language;
            private Object soundcloud_id;
            private Object soundcloud_error_code;
            private Object soundcloud_error_msg;
            private Object soundcloud_link_to_file;
            private Object soundcloud_upload_time;
            private String replay_gain;
            private int owner_id;
            private String cuein;
            private String cueout;
            private boolean hidden;
            private int filesize;
            private Object description;
            private int cloud_file_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMime() {
                return mime;
            }

            public void setMime(String mime) {
                this.mime = mime;
            }

            public String getFtype() {
                return ftype;
            }

            public void setFtype(String ftype) {
                this.ftype = ftype;
            }

            public Object getDirectory() {
                return directory;
            }

            public void setDirectory(Object directory) {
                this.directory = directory;
            }

            public String getFilepath() {
                return filepath;
            }

            public void setFilepath(String filepath) {
                this.filepath = filepath;
            }

            public int getImport_status() {
                return import_status;
            }

            public void setImport_status(int import_status) {
                this.import_status = import_status;
            }

            public int getCurrentlyaccessing() {
                return currentlyaccessing;
            }

            public void setCurrentlyaccessing(int currentlyaccessing) {
                this.currentlyaccessing = currentlyaccessing;
            }

            public Object getEditedby() {
                return editedby;
            }

            public void setEditedby(Object editedby) {
                this.editedby = editedby;
            }

            public String getMtime() {
                return mtime;
            }

            public void setMtime(String mtime) {
                this.mtime = mtime;
            }

            public String getUtime() {
                return utime;
            }

            public void setUtime(String utime) {
                this.utime = utime;
            }

            public String getLptime() {
                return lptime;
            }

            public void setLptime(String lptime) {
                this.lptime = lptime;
            }

            public String getMd5() {
                return md5;
            }

            public void setMd5(String md5) {
                this.md5 = md5;
            }

            public String getTrack_title() {
                return track_title;
            }

            public void setTrack_title(String track_title) {
                this.track_title = track_title;
            }

            public String getArtist_name() {
                return artist_name;
            }

            public void setArtist_name(String artist_name) {
                this.artist_name = artist_name;
            }

            public int getBit_rate() {
                return bit_rate;
            }

            public void setBit_rate(int bit_rate) {
                this.bit_rate = bit_rate;
            }

            public int getSample_rate() {
                return sample_rate;
            }

            public void setSample_rate(int sample_rate) {
                this.sample_rate = sample_rate;
            }

            public Object getFormat() {
                return format;
            }

            public void setFormat(Object format) {
                this.format = format;
            }

            public String getLength() {
                return length;
            }

            public void setLength(String length) {
                this.length = length;
            }

            public String getAlbum_title() {
                return album_title;
            }

            public void setAlbum_title(String album_title) {
                this.album_title = album_title;
            }

            public String getGenre() {
                return genre;
            }

            public void setGenre(String genre) {
                this.genre = genre;
            }

            public String getComments() {
                return comments;
            }

            public void setComments(String comments) {
                this.comments = comments;
            }

            public String getYear() {
                return year;
            }

            public void setYear(String year) {
                this.year = year;
            }

            public int getTrack_number() {
                return track_number;
            }

            public void setTrack_number(int track_number) {
                this.track_number = track_number;
            }

            public int getChannels() {
                return channels;
            }

            public void setChannels(int channels) {
                this.channels = channels;
            }

            public Object getUrl() {
                return url;
            }

            public void setUrl(Object url) {
                this.url = url;
            }

            public Object getBpm() {
                return bpm;
            }

            public void setBpm(Object bpm) {
                this.bpm = bpm;
            }

            public Object getRating() {
                return rating;
            }

            public void setRating(Object rating) {
                this.rating = rating;
            }

            public Object getEncoded_by() {
                return encoded_by;
            }

            public void setEncoded_by(Object encoded_by) {
                this.encoded_by = encoded_by;
            }

            public Object getDisc_number() {
                return disc_number;
            }

            public void setDisc_number(Object disc_number) {
                this.disc_number = disc_number;
            }

            public Object getMood() {
                return mood;
            }

            public void setMood(Object mood) {
                this.mood = mood;
            }

            public Object getLabel() {
                return label;
            }

            public void setLabel(Object label) {
                this.label = label;
            }

            public Object getComposer() {
                return composer;
            }

            public void setComposer(Object composer) {
                this.composer = composer;
            }

            public Object getEncoder() {
                return encoder;
            }

            public void setEncoder(Object encoder) {
                this.encoder = encoder;
            }

            public Object getChecksum() {
                return checksum;
            }

            public void setChecksum(Object checksum) {
                this.checksum = checksum;
            }

            public Object getLyrics() {
                return lyrics;
            }

            public void setLyrics(Object lyrics) {
                this.lyrics = lyrics;
            }

            public Object getOrchestra() {
                return orchestra;
            }

            public void setOrchestra(Object orchestra) {
                this.orchestra = orchestra;
            }

            public Object getConductor() {
                return conductor;
            }

            public void setConductor(Object conductor) {
                this.conductor = conductor;
            }

            public Object getLyricist() {
                return lyricist;
            }

            public void setLyricist(Object lyricist) {
                this.lyricist = lyricist;
            }

            public Object getOriginal_lyricist() {
                return original_lyricist;
            }

            public void setOriginal_lyricist(Object original_lyricist) {
                this.original_lyricist = original_lyricist;
            }

            public Object getRadio_station_name() {
                return radio_station_name;
            }

            public void setRadio_station_name(Object radio_station_name) {
                this.radio_station_name = radio_station_name;
            }

            public Object getInfo_url() {
                return info_url;
            }

            public void setInfo_url(Object info_url) {
                this.info_url = info_url;
            }

            public Object getArtist_url() {
                return artist_url;
            }

            public void setArtist_url(Object artist_url) {
                this.artist_url = artist_url;
            }

            public Object getAudio_source_url() {
                return audio_source_url;
            }

            public void setAudio_source_url(Object audio_source_url) {
                this.audio_source_url = audio_source_url;
            }

            public Object getRadio_station_url() {
                return radio_station_url;
            }

            public void setRadio_station_url(Object radio_station_url) {
                this.radio_station_url = radio_station_url;
            }

            public Object getBuy_this_url() {
                return buy_this_url;
            }

            public void setBuy_this_url(Object buy_this_url) {
                this.buy_this_url = buy_this_url;
            }

            public Object getIsrc_number() {
                return isrc_number;
            }

            public void setIsrc_number(Object isrc_number) {
                this.isrc_number = isrc_number;
            }

            public Object getCatalog_number() {
                return catalog_number;
            }

            public void setCatalog_number(Object catalog_number) {
                this.catalog_number = catalog_number;
            }

            public Object getOriginal_artist() {
                return original_artist;
            }

            public void setOriginal_artist(Object original_artist) {
                this.original_artist = original_artist;
            }

            public Object getCopyright() {
                return copyright;
            }

            public void setCopyright(Object copyright) {
                this.copyright = copyright;
            }

            public Object getReport_datetime() {
                return report_datetime;
            }

            public void setReport_datetime(Object report_datetime) {
                this.report_datetime = report_datetime;
            }

            public Object getReport_location() {
                return report_location;
            }

            public void setReport_location(Object report_location) {
                this.report_location = report_location;
            }

            public Object getReport_organization() {
                return report_organization;
            }

            public void setReport_organization(Object report_organization) {
                this.report_organization = report_organization;
            }

            public Object getSubject() {
                return subject;
            }

            public void setSubject(Object subject) {
                this.subject = subject;
            }

            public Object getContributor() {
                return contributor;
            }

            public void setContributor(Object contributor) {
                this.contributor = contributor;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public Object getSoundcloud_id() {
                return soundcloud_id;
            }

            public void setSoundcloud_id(Object soundcloud_id) {
                this.soundcloud_id = soundcloud_id;
            }

            public Object getSoundcloud_error_code() {
                return soundcloud_error_code;
            }

            public void setSoundcloud_error_code(Object soundcloud_error_code) {
                this.soundcloud_error_code = soundcloud_error_code;
            }

            public Object getSoundcloud_error_msg() {
                return soundcloud_error_msg;
            }

            public void setSoundcloud_error_msg(Object soundcloud_error_msg) {
                this.soundcloud_error_msg = soundcloud_error_msg;
            }

            public Object getSoundcloud_link_to_file() {
                return soundcloud_link_to_file;
            }

            public void setSoundcloud_link_to_file(Object soundcloud_link_to_file) {
                this.soundcloud_link_to_file = soundcloud_link_to_file;
            }

            public Object getSoundcloud_upload_time() {
                return soundcloud_upload_time;
            }

            public void setSoundcloud_upload_time(Object soundcloud_upload_time) {
                this.soundcloud_upload_time = soundcloud_upload_time;
            }

            public String getReplay_gain() {
                return replay_gain;
            }

            public void setReplay_gain(String replay_gain) {
                this.replay_gain = replay_gain;
            }

            public int getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(int owner_id) {
                this.owner_id = owner_id;
            }

            public String getCuein() {
                return cuein;
            }

            public void setCuein(String cuein) {
                this.cuein = cuein;
            }

            public String getCueout() {
                return cueout;
            }

            public void setCueout(String cueout) {
                this.cueout = cueout;
            }

            public boolean isHidden() {
                return hidden;
            }

            public void setHidden(boolean hidden) {
                this.hidden = hidden;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public Object getDescription() {
                return description;
            }

            public void setDescription(Object description) {
                this.description = description;
            }

            public int getCloud_file_id() {
                return cloud_file_id;
            }

            public void setCloud_file_id(int cloud_file_id) {
                this.cloud_file_id = cloud_file_id;
            }
        }
    }

    public static class NextBean {
        /**
         * starts : 2017-03-21 10:18:47.000000
         * ends : 2017-03-21 10:24:42.000000
         * type : track
         * name : Jose Cid - Verdes trigais em flor
         * metadata : {"id":37628,"name":"","mime":"audio/mp3","ftype":"audioclip","directory":null,"filepath":"13 Verdes trigais em flor.mp3","import_status":0,"currentlyaccessing":0,"editedby":null,"mtime":"2016-07-27 00:18:23","utime":"2016-07-27 00:18:18","lptime":"2017-03-21 08:25:57","md5":"0943cf66f9096734180e7740f6185a52","track_title":"Verdes trigais em flor","artist_name":"Jose Cid","bit_rate":256000,"sample_rate":44100,"format":null,"length":"00:05:59.941188","album_title":"O melhor dos melhores","genre":"Portuguesa","comments":"","year":null,"track_number":13,"channels":2,"url":null,"bpm":null,"rating":null,"encoded_by":null,"disc_number":null,"mood":null,"label":null,"composer":null,"encoder":null,"checksum":null,"lyrics":null,"orchestra":null,"conductor":null,"lyricist":null,"original_lyricist":null,"radio_station_name":null,"info_url":null,"artist_url":null,"audio_source_url":null,"radio_station_url":null,"buy_this_url":null,"isrc_number":null,"catalog_number":null,"original_artist":null,"copyright":null,"report_datetime":null,"report_location":null,"report_organization":null,"subject":null,"contributor":null,"language":null,"soundcloud_id":null,"soundcloud_error_code":null,"soundcloud_error_msg":null,"soundcloud_link_to_file":null,"soundcloud_upload_time":null,"replay_gain":"-1.27","owner_id":1,"cuein":"00:00:00.996531","cueout":"00:05:56.482404","hidden":false,"filesize":11522470,"description":null,"cloud_file_id":18169}
         */

        private String starts;
        private String ends;
        private String type;
        private String name;
        private MetadataBeanXX metadata;

        public String getStarts() {
            return starts;
        }

        public void setStarts(String starts) {
            this.starts = starts;
        }

        public String getEnds() {
            return ends;
        }

        public void setEnds(String ends) {
            this.ends = ends;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public MetadataBeanXX getMetadata() {
            return metadata;
        }

        public void setMetadata(MetadataBeanXX metadata) {
            this.metadata = metadata;
        }

        public static class MetadataBeanXX {
            /**
             * id : 37628
             * name :
             * mime : audio/mp3
             * ftype : audioclip
             * directory : null
             * filepath : 13 Verdes trigais em flor.mp3
             * import_status : 0
             * currentlyaccessing : 0
             * editedby : null
             * mtime : 2016-07-27 00:18:23
             * utime : 2016-07-27 00:18:18
             * lptime : 2017-03-21 08:25:57
             * md5 : 0943cf66f9096734180e7740f6185a52
             * track_title : Verdes trigais em flor
             * artist_name : Jose Cid
             * bit_rate : 256000
             * sample_rate : 44100
             * format : null
             * length : 00:05:59.941188
             * album_title : O melhor dos melhores
             * genre : Portuguesa
             * comments :
             * year : null
             * track_number : 13
             * channels : 2
             * url : null
             * bpm : null
             * rating : null
             * encoded_by : null
             * disc_number : null
             * mood : null
             * label : null
             * composer : null
             * encoder : null
             * checksum : null
             * lyrics : null
             * orchestra : null
             * conductor : null
             * lyricist : null
             * original_lyricist : null
             * radio_station_name : null
             * info_url : null
             * artist_url : null
             * audio_source_url : null
             * radio_station_url : null
             * buy_this_url : null
             * isrc_number : null
             * catalog_number : null
             * original_artist : null
             * copyright : null
             * report_datetime : null
             * report_location : null
             * report_organization : null
             * subject : null
             * contributor : null
             * language : null
             * soundcloud_id : null
             * soundcloud_error_code : null
             * soundcloud_error_msg : null
             * soundcloud_link_to_file : null
             * soundcloud_upload_time : null
             * replay_gain : -1.27
             * owner_id : 1
             * cuein : 00:00:00.996531
             * cueout : 00:05:56.482404
             * hidden : false
             * filesize : 11522470
             * description : null
             * cloud_file_id : 18169
             */

            private int id;
            private String name;
            private String mime;
            private String ftype;
            private Object directory;
            private String filepath;
            private int import_status;
            private int currentlyaccessing;
            private Object editedby;
            private String mtime;
            private String utime;
            private String lptime;
            private String md5;
            private String track_title;
            private String artist_name;
            private int bit_rate;
            private int sample_rate;
            private Object format;
            private String length;
            private String album_title;
            private String genre;
            private String comments;
            private Object year;
            private int track_number;
            private int channels;
            private Object url;
            private Object bpm;
            private Object rating;
            private Object encoded_by;
            private Object disc_number;
            private Object mood;
            private Object label;
            private Object composer;
            private Object encoder;
            private Object checksum;
            private Object lyrics;
            private Object orchestra;
            private Object conductor;
            private Object lyricist;
            private Object original_lyricist;
            private Object radio_station_name;
            private Object info_url;
            private Object artist_url;
            private Object audio_source_url;
            private Object radio_station_url;
            private Object buy_this_url;
            private Object isrc_number;
            private Object catalog_number;
            private Object original_artist;
            private Object copyright;
            private Object report_datetime;
            private Object report_location;
            private Object report_organization;
            private Object subject;
            private Object contributor;
            private Object language;
            private Object soundcloud_id;
            private Object soundcloud_error_code;
            private Object soundcloud_error_msg;
            private Object soundcloud_link_to_file;
            private Object soundcloud_upload_time;
            private String replay_gain;
            private int owner_id;
            private String cuein;
            private String cueout;
            private boolean hidden;
            private int filesize;
            private Object description;
            private int cloud_file_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMime() {
                return mime;
            }

            public void setMime(String mime) {
                this.mime = mime;
            }

            public String getFtype() {
                return ftype;
            }

            public void setFtype(String ftype) {
                this.ftype = ftype;
            }

            public Object getDirectory() {
                return directory;
            }

            public void setDirectory(Object directory) {
                this.directory = directory;
            }

            public String getFilepath() {
                return filepath;
            }

            public void setFilepath(String filepath) {
                this.filepath = filepath;
            }

            public int getImport_status() {
                return import_status;
            }

            public void setImport_status(int import_status) {
                this.import_status = import_status;
            }

            public int getCurrentlyaccessing() {
                return currentlyaccessing;
            }

            public void setCurrentlyaccessing(int currentlyaccessing) {
                this.currentlyaccessing = currentlyaccessing;
            }

            public Object getEditedby() {
                return editedby;
            }

            public void setEditedby(Object editedby) {
                this.editedby = editedby;
            }

            public String getMtime() {
                return mtime;
            }

            public void setMtime(String mtime) {
                this.mtime = mtime;
            }

            public String getUtime() {
                return utime;
            }

            public void setUtime(String utime) {
                this.utime = utime;
            }

            public String getLptime() {
                return lptime;
            }

            public void setLptime(String lptime) {
                this.lptime = lptime;
            }

            public String getMd5() {
                return md5;
            }

            public void setMd5(String md5) {
                this.md5 = md5;
            }

            public String getTrack_title() {
                return track_title;
            }

            public void setTrack_title(String track_title) {
                this.track_title = track_title;
            }

            public String getArtist_name() {
                return artist_name;
            }

            public void setArtist_name(String artist_name) {
                this.artist_name = artist_name;
            }

            public int getBit_rate() {
                return bit_rate;
            }

            public void setBit_rate(int bit_rate) {
                this.bit_rate = bit_rate;
            }

            public int getSample_rate() {
                return sample_rate;
            }

            public void setSample_rate(int sample_rate) {
                this.sample_rate = sample_rate;
            }

            public Object getFormat() {
                return format;
            }

            public void setFormat(Object format) {
                this.format = format;
            }

            public String getLength() {
                return length;
            }

            public void setLength(String length) {
                this.length = length;
            }

            public String getAlbum_title() {
                return album_title;
            }

            public void setAlbum_title(String album_title) {
                this.album_title = album_title;
            }

            public String getGenre() {
                return genre;
            }

            public void setGenre(String genre) {
                this.genre = genre;
            }

            public String getComments() {
                return comments;
            }

            public void setComments(String comments) {
                this.comments = comments;
            }

            public Object getYear() {
                return year;
            }

            public void setYear(Object year) {
                this.year = year;
            }

            public int getTrack_number() {
                return track_number;
            }

            public void setTrack_number(int track_number) {
                this.track_number = track_number;
            }

            public int getChannels() {
                return channels;
            }

            public void setChannels(int channels) {
                this.channels = channels;
            }

            public Object getUrl() {
                return url;
            }

            public void setUrl(Object url) {
                this.url = url;
            }

            public Object getBpm() {
                return bpm;
            }

            public void setBpm(Object bpm) {
                this.bpm = bpm;
            }

            public Object getRating() {
                return rating;
            }

            public void setRating(Object rating) {
                this.rating = rating;
            }

            public Object getEncoded_by() {
                return encoded_by;
            }

            public void setEncoded_by(Object encoded_by) {
                this.encoded_by = encoded_by;
            }

            public Object getDisc_number() {
                return disc_number;
            }

            public void setDisc_number(Object disc_number) {
                this.disc_number = disc_number;
            }

            public Object getMood() {
                return mood;
            }

            public void setMood(Object mood) {
                this.mood = mood;
            }

            public Object getLabel() {
                return label;
            }

            public void setLabel(Object label) {
                this.label = label;
            }

            public Object getComposer() {
                return composer;
            }

            public void setComposer(Object composer) {
                this.composer = composer;
            }

            public Object getEncoder() {
                return encoder;
            }

            public void setEncoder(Object encoder) {
                this.encoder = encoder;
            }

            public Object getChecksum() {
                return checksum;
            }

            public void setChecksum(Object checksum) {
                this.checksum = checksum;
            }

            public Object getLyrics() {
                return lyrics;
            }

            public void setLyrics(Object lyrics) {
                this.lyrics = lyrics;
            }

            public Object getOrchestra() {
                return orchestra;
            }

            public void setOrchestra(Object orchestra) {
                this.orchestra = orchestra;
            }

            public Object getConductor() {
                return conductor;
            }

            public void setConductor(Object conductor) {
                this.conductor = conductor;
            }

            public Object getLyricist() {
                return lyricist;
            }

            public void setLyricist(Object lyricist) {
                this.lyricist = lyricist;
            }

            public Object getOriginal_lyricist() {
                return original_lyricist;
            }

            public void setOriginal_lyricist(Object original_lyricist) {
                this.original_lyricist = original_lyricist;
            }

            public Object getRadio_station_name() {
                return radio_station_name;
            }

            public void setRadio_station_name(Object radio_station_name) {
                this.radio_station_name = radio_station_name;
            }

            public Object getInfo_url() {
                return info_url;
            }

            public void setInfo_url(Object info_url) {
                this.info_url = info_url;
            }

            public Object getArtist_url() {
                return artist_url;
            }

            public void setArtist_url(Object artist_url) {
                this.artist_url = artist_url;
            }

            public Object getAudio_source_url() {
                return audio_source_url;
            }

            public void setAudio_source_url(Object audio_source_url) {
                this.audio_source_url = audio_source_url;
            }

            public Object getRadio_station_url() {
                return radio_station_url;
            }

            public void setRadio_station_url(Object radio_station_url) {
                this.radio_station_url = radio_station_url;
            }

            public Object getBuy_this_url() {
                return buy_this_url;
            }

            public void setBuy_this_url(Object buy_this_url) {
                this.buy_this_url = buy_this_url;
            }

            public Object getIsrc_number() {
                return isrc_number;
            }

            public void setIsrc_number(Object isrc_number) {
                this.isrc_number = isrc_number;
            }

            public Object getCatalog_number() {
                return catalog_number;
            }

            public void setCatalog_number(Object catalog_number) {
                this.catalog_number = catalog_number;
            }

            public Object getOriginal_artist() {
                return original_artist;
            }

            public void setOriginal_artist(Object original_artist) {
                this.original_artist = original_artist;
            }

            public Object getCopyright() {
                return copyright;
            }

            public void setCopyright(Object copyright) {
                this.copyright = copyright;
            }

            public Object getReport_datetime() {
                return report_datetime;
            }

            public void setReport_datetime(Object report_datetime) {
                this.report_datetime = report_datetime;
            }

            public Object getReport_location() {
                return report_location;
            }

            public void setReport_location(Object report_location) {
                this.report_location = report_location;
            }

            public Object getReport_organization() {
                return report_organization;
            }

            public void setReport_organization(Object report_organization) {
                this.report_organization = report_organization;
            }

            public Object getSubject() {
                return subject;
            }

            public void setSubject(Object subject) {
                this.subject = subject;
            }

            public Object getContributor() {
                return contributor;
            }

            public void setContributor(Object contributor) {
                this.contributor = contributor;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public Object getSoundcloud_id() {
                return soundcloud_id;
            }

            public void setSoundcloud_id(Object soundcloud_id) {
                this.soundcloud_id = soundcloud_id;
            }

            public Object getSoundcloud_error_code() {
                return soundcloud_error_code;
            }

            public void setSoundcloud_error_code(Object soundcloud_error_code) {
                this.soundcloud_error_code = soundcloud_error_code;
            }

            public Object getSoundcloud_error_msg() {
                return soundcloud_error_msg;
            }

            public void setSoundcloud_error_msg(Object soundcloud_error_msg) {
                this.soundcloud_error_msg = soundcloud_error_msg;
            }

            public Object getSoundcloud_link_to_file() {
                return soundcloud_link_to_file;
            }

            public void setSoundcloud_link_to_file(Object soundcloud_link_to_file) {
                this.soundcloud_link_to_file = soundcloud_link_to_file;
            }

            public Object getSoundcloud_upload_time() {
                return soundcloud_upload_time;
            }

            public void setSoundcloud_upload_time(Object soundcloud_upload_time) {
                this.soundcloud_upload_time = soundcloud_upload_time;
            }

            public String getReplay_gain() {
                return replay_gain;
            }

            public void setReplay_gain(String replay_gain) {
                this.replay_gain = replay_gain;
            }

            public int getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(int owner_id) {
                this.owner_id = owner_id;
            }

            public String getCuein() {
                return cuein;
            }

            public void setCuein(String cuein) {
                this.cuein = cuein;
            }

            public String getCueout() {
                return cueout;
            }

            public void setCueout(String cueout) {
                this.cueout = cueout;
            }

            public boolean isHidden() {
                return hidden;
            }

            public void setHidden(boolean hidden) {
                this.hidden = hidden;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public Object getDescription() {
                return description;
            }

            public void setDescription(Object description) {
                this.description = description;
            }

            public int getCloud_file_id() {
                return cloud_file_id;
            }

            public void setCloud_file_id(int cloud_file_id) {
                this.cloud_file_id = cloud_file_id;
            }
        }
    }

    public static class CurrentShowBean {
        /**
         * start_timestamp : 2017-03-21 06:00:00
         * end_timestamp : 2017-03-21 07:00:00
         * name : Musica Variada
         * description :
         * id : 3155
         * instance_id : 6512
         * record : 0
         * url :
         * image_path :
         * image_cloud_file_id : null
         * auto_dj : true
         * starts : 2017-03-21 06:00:00
         * ends : 2017-03-21 07:00:00
         */

        private String start_timestamp;
        private String end_timestamp;
        private String name;
        private String description;
        private int id;
        private int instance_id;
        private int record;
        private String url;
        private String image_path;
        private Object image_cloud_file_id;
        private boolean auto_dj;
        private String starts;
        private String ends;

        public String getStart_timestamp() {
            return start_timestamp;
        }

        public void setStart_timestamp(String start_timestamp) {
            this.start_timestamp = start_timestamp;
        }

        public String getEnd_timestamp() {
            return end_timestamp;
        }

        public void setEnd_timestamp(String end_timestamp) {
            this.end_timestamp = end_timestamp;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getInstance_id() {
            return instance_id;
        }

        public void setInstance_id(int instance_id) {
            this.instance_id = instance_id;
        }

        public int getRecord() {
            return record;
        }

        public void setRecord(int record) {
            this.record = record;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImage_path() {
            return image_path;
        }

        public void setImage_path(String image_path) {
            this.image_path = image_path;
        }

        public Object getImage_cloud_file_id() {
            return image_cloud_file_id;
        }

        public void setImage_cloud_file_id(Object image_cloud_file_id) {
            this.image_cloud_file_id = image_cloud_file_id;
        }

        public boolean isAuto_dj() {
            return auto_dj;
        }

        public void setAuto_dj(boolean auto_dj) {
            this.auto_dj = auto_dj;
        }

        public String getStarts() {
            return starts;
        }

        public void setStarts(String starts) {
            this.starts = starts;
        }

        public String getEnds() {
            return ends;
        }

        public void setEnds(String ends) {
            this.ends = ends;
        }
    }

    public static class NextShowBean {
        /**
         * id : 2099
         * instance_id : 5832
         * name : CamoesRadioCKWR 5 to 8 pm (Repetiçao)
         * description :
         * url : www.camoesradio.com
         * start_timestamp : 2017-03-21 07:00:00
         * end_timestamp : 2017-03-21 10:00:00
         * starts : 2017-03-21 07:00:00
         * ends : 2017-03-21 10:00:00
         * record : 0
         * image_path :
         * image_cloud_file_id : null
         * auto_dj : false
         * type : show
         */

        private int id;
        private int instance_id;
        private String name;
        private String description;
        private String url;
        private String start_timestamp;
        private String end_timestamp;
        private String starts;
        private String ends;
        private int record;
        private String image_path;
        private Object image_cloud_file_id;
        private boolean auto_dj;
        private String type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getInstance_id() {
            return instance_id;
        }

        public void setInstance_id(int instance_id) {
            this.instance_id = instance_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getStart_timestamp() {
            return start_timestamp;
        }

        public void setStart_timestamp(String start_timestamp) {
            this.start_timestamp = start_timestamp;
        }

        public String getEnd_timestamp() {
            return end_timestamp;
        }

        public void setEnd_timestamp(String end_timestamp) {
            this.end_timestamp = end_timestamp;
        }

        public String getStarts() {
            return starts;
        }

        public void setStarts(String starts) {
            this.starts = starts;
        }

        public String getEnds() {
            return ends;
        }

        public void setEnds(String ends) {
            this.ends = ends;
        }

        public int getRecord() {
            return record;
        }

        public void setRecord(int record) {
            this.record = record;
        }

        public String getImage_path() {
            return image_path;
        }

        public void setImage_path(String image_path) {
            this.image_path = image_path;
        }

        public Object getImage_cloud_file_id() {
            return image_cloud_file_id;
        }

        public void setImage_cloud_file_id(Object image_cloud_file_id) {
            this.image_cloud_file_id = image_cloud_file_id;
        }

        public boolean isAuto_dj() {
            return auto_dj;
        }

        public void setAuto_dj(boolean auto_dj) {
            this.auto_dj = auto_dj;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
