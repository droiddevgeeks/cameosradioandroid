package com.camoesradio.mobile1.fonts;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Created by BQ 4 on 5/31/2017.
 * workaround for serious memory issue in pre-4.0 devices
 */

public class TypeFaces {

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(Context c, String name) {
        synchronized (cache) {
            if(cache.containsKey(name)) {
                Typeface t = Typeface.createFromAsset(c.getAssets(), String.format("%s.ttf", name));
                cache.put(name, t);
            }
            return cache.get(name);
        }
    }
}