package com.camoesradio.mobile1.main;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.camoesradio.mobile1.R;
import com.camoesradio.mobile1.bean.response.GetApiResponse;
import com.camoesradio.mobile1.retrofit.ProjectAPI;
import com.camoesradio.mobile1.retrofit.RetrofitService;
import com.camoesradio.mobile1.service.MusicService;
import com.camoesradio.mobile1.service.constant.BroadcastAction;
import com.camoesradio.mobile1.service.constant.BundleArg;
import com.camoesradio.mobile1.service.constant.HandlerAction;
import com.camoesradio.mobile1.service.util.ServiceCallbacks;
import com.camoesradio.mobile1.utils.AppConstants;
import com.camoesradio.mobile1.utils.HelperMethods;
import com.camoesradio.mobile1.utils.Keys;
import com.camoesradio.mobile1.utils.ServiceUrls;

import java.io.IOException;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ServiceConnection, ServiceCallbacks {

    private final String TAG = MainActivity.class.getName();
    private ImageView /*imageViewFb, imageViewTwitter, imageViewYoutube, imageViewMenu,*/
            imageViewPrev, imageViewPlay, imageViewNext, imageViewAdd, imageViewMenu, imageViewBottomBanner;
    private SeekBar soundBar;
    private PopupWindow menuPopupWindow;
    private TextView textViewSchedule, textViewOurShows, textViewOurBlogs, textViewCamoestv, textViewAboutus, textViewContactus, textViewSongName;
    private MediaPlayer mp;
    private boolean isPlay;
    private AudioManager mAudioManager;
    private LinearLayout layoutFb, layoutTwitter, layoutYoutube, layoutMenu;
    private Subscription subscription;
    private Messenger mPlayerMessenger;
    private boolean mIsBound = false;
    String[] radioStreamingList = {ServiceUrls.LIVE_STREAM, ServiceUrls.NEXT_SONG};
    int currentPlayingPosition = 0;
    private ProgressBar progressBar;

    Observer<GetApiResponse> myObserver = new Observer<GetApiResponse>() {

        @Override
        public void onCompleted() {
            subscription.unsubscribe();
        }

        @Override
        public void onError(Throwable e) {
            // Called when the observable encounters an error
            Log.d(TAG, ">>>> onError gets called : " + e.getMessage());
        }

        @Override
        public void onNext(GetApiResponse getApiResponse) {

            Log.e(TAG, " onNext " + getApiResponse.getCurrent().getName());
            textViewSongName.setText(getApiResponse.getCurrent().getName() + "");
            //  findViewAndSetAdapter(geoNamesApi);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        initService();
        initViews();
        initListeners();
        menuDialog();
        setMusicSound();
        //    callMusicService();
    }

    private void setMusicSound() {
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        final int originalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        soundBar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        soundBar.setProgress(originalVolume);
    }

    private void initViews() {
        mp = new MediaPlayer();
        // new LoadMusic().execute(ServiceUrls.LIVE_STREAM);
       /* imageViewFb = (ImageView) findViewById(R.id.imageViewAdd);
        imageViewTwitter = (ImageView) findViewById(R.id.imageViewTwitter);
        imageViewYoutube = (ImageView) findViewById(R.id.imageViewYoutube);
        imageViewMenu = (ImageView) findViewById(R.id.imageViewMenu);*/
        textViewSongName = (TextView) findViewById(R.id.textViewSongName);
        textViewSongName.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));
        textViewSongName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        imageViewPrev = (ImageView) findViewById(R.id.imageViewPrev);
        imageViewPlay = (ImageView) findViewById(R.id.imageViewPlay);
        imageViewNext = (ImageView) findViewById(R.id.imageViewNext);
        imageViewAdd = (ImageView) findViewById(R.id.imageViewAdd);
        imageViewBottomBanner = (ImageView) findViewById(R.id.imageViewBottomBanner);
        imageViewMenu = (ImageView) findViewById(R.id.imageViewMenu);

        layoutFb = (LinearLayout) findViewById(R.id.layoutFb);
        layoutTwitter = (LinearLayout) findViewById(R.id.layoutTwitter);
        layoutYoutube = (LinearLayout) findViewById(R.id.layoutYoutube);
        layoutMenu = (LinearLayout) findViewById(R.id.layoutMenu);

        soundBar = (SeekBar) findViewById(R.id.soundBar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


    }

    private void initListeners() {

     /*   imageViewFb.setOnClickListener(this);
        imageViewTwitter.setOnClickListener(this);
        imageViewYoutube.setOnClickListener(this);
        imageViewMenu.setOnClickListener(this);*/
        imageViewPrev.setOnClickListener(this);
        imageViewPlay.setOnClickListener(this);
        imageViewNext.setOnClickListener(this);
        imageViewAdd.setOnClickListener(this);
        imageViewBottomBanner.setOnClickListener(this);
        layoutFb.setOnClickListener(this);
        layoutTwitter.setOnClickListener(this);
        layoutYoutube.setOnClickListener(this);
        layoutMenu.setOnClickListener(this);

        soundBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.layoutFb:
                openUrl(AppConstants.FB_URL);
                break;
            case R.id.layoutTwitter:
                openUrl(AppConstants.TWITTER_URL);
                break;
            case R.id.layoutYoutube:
                openUrl(AppConstants.YOUTUBE_URL);
                break;
            case R.id.layoutMenu:
                if (menuPopupWindow.isShowing()) {
                    menuPopupWindow.dismiss();
                } else {
                    menuPopupWindow.showAsDropDown(layoutMenu, 0, -layoutMenu.getHeight() / 3);
                }
                break;
            case R.id.imageViewPlay:
                if (!isPlay) {
                    //  stopMusic();
                    // new LoadMusic().execute(radioStreamingList[currentPlayingPosition]);
                    playMusic(radioStreamingList[currentPlayingPosition]);

                    // mp.start();
                    isPlay = true;
                    imageViewPlay.setImageResource(R.drawable.pause_icon);
                } else {
                    //  mp.pause();
                    // stopMusic();
                    pauseMusic();

                    isPlay = false;
                    imageViewPlay.setImageResource(R.drawable.play_icon);

                }
                break;
            case R.id.imageViewPrev:
                if (currentPlayingPosition < radioStreamingList.length && currentPlayingPosition > 0) {
                    currentPlayingPosition = currentPlayingPosition - 1;
                    Log.e(TAG, "Prev :: " + currentPlayingPosition + radioStreamingList[currentPlayingPosition]);
                    //  stopMusic();
                    playMusic(radioStreamingList[currentPlayingPosition]);

                }
                break;
            case R.id.imageViewNext:
                if (currentPlayingPosition < radioStreamingList.length - 1) {
                    currentPlayingPosition = currentPlayingPosition + 1;
                    Log.e(TAG, "Next :: " + currentPlayingPosition + radioStreamingList[currentPlayingPosition]);
                    // stopMusic();
                    playMusic(radioStreamingList[currentPlayingPosition]);
                }

                break;
            case R.id.textViewSchedule:
                menuPopupWindow.dismiss();
                openUrl(AppConstants.SCHEDULE_URL);
                break;
            case R.id.textViewOurShows:
                menuPopupWindow.dismiss();
                openUrl(AppConstants.OUR_SHOWS_URL);
                break;
            case R.id.textViewOurBlogs:
                menuPopupWindow.dismiss();
                openUrl(AppConstants.OUR_BLOG_URL);
                break;
            case R.id.textViewCamoestv:
                menuPopupWindow.dismiss();
                openUrl(AppConstants.CAMOES_TV_URL);
                break;
            case R.id.textViewAboutus:
                menuPopupWindow.dismiss();
                openUrl(AppConstants.ABOUT_US_URL);
                break;
            case R.id.textViewContactus:
                menuPopupWindow.dismiss();
                sendEmail();
                break;
            case R.id.imageViewAdd:
                openUrl(AppConstants.TOP_BANNER);
                break;
            case R.id.imageViewBottomBanner:
                openUrl(AppConstants.BOTTOM_BANNER);
                break;
        }
    }

    /**
     * method for play next song
     */
    private void playNextSong() {
        if (mp != null) {
            if (mp.isPlaying())
                mp.stop();
            mp.reset();
        }
        try {

            mp.setDataSource(ServiceUrls.NEXT_SONG);
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.prepare();
            mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void callMusicService() {

        if (HelperMethods.isConnectingToInternet(this)) {

            final ProjectAPI service = RetrofitService.createRetrofitClient();

            subscription = service.getAllMusicData()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(myObserver);
        } else {

            HelperMethods.showToastS(this, "Your are not connected to internet.");


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (menuPopupWindow.isShowing())
            menuPopupWindow.dismiss();
        if (mp != null) {
            if (mp.isPlaying())
                mp.stop();
            mp.release();
        }
        unbindService(this);
    }

    /**
     * method to show option menu
     */
    private void menuDialog() {
        LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View view = mInflater.inflate(R.layout.menu_dialog, null);
        //   view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        textViewSchedule = (TextView) view.findViewById(R.id.textViewSchedule);
        textViewOurShows = (TextView) view.findViewById(R.id.textViewOurShows);
        textViewOurBlogs = (TextView) view.findViewById(R.id.textViewOurBlogs);
        textViewCamoestv = (TextView) view.findViewById(R.id.textViewCamoestv);
        textViewAboutus = (TextView) view.findViewById(R.id.textViewAboutus);
        textViewContactus = (TextView) view.findViewById(R.id.textViewContactus);

        textViewSchedule.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));
        textViewOurShows.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));
        textViewOurBlogs.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));
        textViewCamoestv.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));
        textViewAboutus.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));
        textViewContactus.setTypeface(HelperMethods.getTypeFaceOpenSans(MainActivity.this));

        textViewSchedule.setOnClickListener(this);
        textViewOurShows.setOnClickListener(this);
        textViewOurBlogs.setOnClickListener(this);
        textViewCamoestv.setOnClickListener(this);
        textViewAboutus.setOnClickListener(this);
        textViewContactus.setOnClickListener(this);


        if (menuPopupWindow == null) {
            menuPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            menuPopupWindow.setOutsideTouchable(true);
            menuPopupWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            menuPopupWindow.setFocusable(true);
            menuPopupWindow.setTouchable(true);
        }

    }

    /**
     * method to open yrl in browser
     * @param url url which need to open in browser
     */

    private void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    private void sendEmail() {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{AppConstants.CONTACT_US_EMAIL_ID});
       /* i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT   , "body of email");*/
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }


    private class LoadMusic extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this, R.style.MyTheme);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {
            String stringUri = params[0];
            Uri uri = Uri.parse(stringUri);
            Message msg = Message.obtain();
            msg.what = HandlerAction.PLAY_URI;
            Bundle data = new Bundle();
            data.putParcelable(BundleArg.PLAY_URI, uri);
            msg.setData(data);
            sendMessage(msg);
           /* try {
                mp.setDataSource(url);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepare();
            } catch (Exception e) {
                Log.i("Exception", "Exception in streaming mediaplayer e = " + e);
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }

    }


    /** below code for meta data */


    private void initService() {
        bindService(new Intent(this, MusicService.class), this, Context.BIND_AUTO_CREATE);
    }


    private BroadcastReceiver mStreamTitleReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String streamTitle = intent.getStringExtra(BundleArg.STREAM_TITLE);
            Log.e(TAG, streamTitle);
            textViewSongName.setText(streamTitle);
        }
    };

    private BroadcastReceiver mHeadersReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          /*  tv_br.setText(intent.getStringExtra(BundleArg.HEADER_BR));
            tv_name.setText(intent.getStringExtra(BundleArg.HEADER_NAME));
            tv_genre.setText(intent.getStringExtra(BundleArg.HEADER_GENRE));
            tv_info.setText(intent.getStringExtra(BundleArg.HEADER_INFO));
            tv_desc.setText(intent.getStringExtra(BundleArg.HEADER_DESCRIPTION));*/
        }
    };

    private BroadcastReceiver mProgressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String progress_Status = intent.getStringExtra(Keys.PROGRESS_STATUS);
            if (progress_Status.equalsIgnoreCase(Keys.HIDE)) {
                hideProgress();
            } else if (progress_Status.equalsIgnoreCase(Keys.SHOW)) {
                showProgress();
            } else if (progress_Status.equalsIgnoreCase(Keys.ERROR)) {
                errorOnPlay();
            } else if (progress_Status.equalsIgnoreCase(Keys.CHECK_INTERNET)) {
                checkInternet();
            }
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mPlayerMessenger = new Messenger(service);
        mIsBound = true;


    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mIsBound = false;
        mPlayerMessenger = null;
    }

    private void playMusic(String stringUri) {
        Log.e(TAG, stringUri);
        Uri uri = Uri.parse(stringUri);
        Message msg = Message.obtain();
        msg.what = HandlerAction.PLAY_URI;
        Bundle data = new Bundle();
        data.putParcelable(BundleArg.PLAY_URI, uri);
        msg.setData(data);
        sendMessage(msg);
    }

    private void stopMusic() {

        Message msg = Message.obtain();
        msg.what = HandlerAction.STOP;
        sendMessage(msg);
    }

    private void pauseMusic() {
        Message msg = Message.obtain();
        msg.what = HandlerAction.PAUSE;
        sendMessage(msg);
    }

    private void playMusic() {
        Message msg = Message.obtain();
        msg.what = HandlerAction.PLAY;
        sendMessage(msg);
    }

    private void errorOnPlay() {
        if (!HelperMethods.isConnectingToInternet(this)) {
            Toast.makeText(this, "Internet Connection is not available", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Error while playing music. Please Try Again!", Toast.LENGTH_SHORT).show();
        }

        isPlay = false;
        imageViewPlay.setImageResource(R.drawable.play_icon);
        Message msg = Message.obtain();
        msg.what = HandlerAction.STOP;
        sendMessage(msg);
    }


    private void checkInternet() {

        /*if (!HelperMethods.isConnectingToInternet(this)) {
            Toast.makeText(this, "Internet Connection is not available", Toast.LENGTH_SHORT).show();
        }*/

        isPlay = false;
        imageViewPlay.setImageResource(R.drawable.play_icon);
        Message msg = Message.obtain();
        msg.what = HandlerAction.PAUSE;
        sendMessage(msg);
    }


    private void sendMessage(Message msg) {
        if (mIsBound && mPlayerMessenger != null) {
            try {
                mPlayerMessenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        registerReceiver(mStreamTitleReceiver, new IntentFilter(BroadcastAction.STREAM_TITLE));
        registerReceiver(mHeadersReceiver, new IntentFilter(BroadcastAction.HEADERS));
        registerReceiver(mProgressReceiver, new IntentFilter(BroadcastAction.PROGRESS));
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(mStreamTitleReceiver);
        unregisterReceiver(mHeadersReceiver);
        unregisterReceiver(mProgressReceiver);
    }

    @Override
    public void onBackPressed() {
        if (MusicService.mIsPlaying) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }

    }
}
