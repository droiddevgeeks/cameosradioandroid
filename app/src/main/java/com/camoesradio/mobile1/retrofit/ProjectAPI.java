package com.camoesradio.mobile1.retrofit;


import com.camoesradio.mobile1.bean.response.GetApiResponse;

import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by sumit singh on 22/03/17.
 */
public interface ProjectAPI {

    @POST("live-info")
    Observable<GetApiResponse> getAllMusicData();

}
