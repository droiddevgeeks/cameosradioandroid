package com.camoesradio.mobile1.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;

import com.camoesradio.mobile1.service.constant.BroadcastAction;
import com.camoesradio.mobile1.service.constant.BundleArg;
import com.camoesradio.mobile1.service.util.IPlayer;
import com.camoesradio.mobile1.service.util.MusicHandler;
import com.camoesradio.mobile1.utils.Keys;
import com.camoesradio.mobile1.utils.ServiceUrls;
import com.vodyasov.amr.AudiostreamMetadataManager;
import com.vodyasov.amr.OnNewMetadataListener;
import com.vodyasov.amr.UserAgent;

import java.io.IOException;
import java.util.List;


public class MusicService extends Service implements
        IPlayer,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener,
        OnNewMetadataListener {
    public static final String LOG_TAG = MusicService.class.getSimpleName();

    private Context mContext;
    private Messenger mMessenger;
    private MediaPlayer mPlayer;
    private Uri mUri;
    public static boolean mIsPlaying = false;
    private long start = 0;
    private static boolean isPrepared;
    private static boolean isStarted;
    private static boolean isError;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplication().getApplicationContext();
        mMessenger = new Messenger(new MusicHandler(this));

        mPlayer = new MediaPlayer();
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnCompletionListener(this);

        try {
            Log.e(LOG_TAG, "-----onCreate------");
            start = System.currentTimeMillis();
            mUri = Uri.parse(ServiceUrls.LIVE_STREAM);
            mPlayer.setDataSource(mContext, mUri);
            mPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onDestroy() {
        if (!mIsPlaying) {
            super.onDestroy();
            mPlayer.release();
            isPrepared = false;
            isError = false;
            Log.e(LOG_TAG, "onDestory");
        }
    }

    @Override
    public void play(Uri uri) {
        if (uri == null) {
            Log.e(LOG_TAG, "Uri must be non-empty");
            return;
        }
        mUri = uri;
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        if (!mUri.equals(uri)) {
            stop();
            try {
                Log.e(LOG_TAG, "-----if play call------");
                showProgress();
                //isStarted = true;
                start = System.currentTimeMillis();
                mPlayer.setDataSource(mContext, mUri);
                mPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(LOG_TAG, "----- else play call------");
            try {

                Log.v(LOG_TAG, String.format("Play: Uri - %s", uri));

                if (isPrepared) {
                    Log.e(LOG_TAG, "-----isPrepared call------");
                    mPlayer.start();
                } else if (isError) {
                    try {
                        Log.e(LOG_TAG, "-----isError call------");
                        showProgress();
                        isStarted = true;
                        start = System.currentTimeMillis();
                        mPlayer.setDataSource(mContext, mUri);
                        mPlayer.prepareAsync();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(LOG_TAG, "-----else call------");
                    showProgress();
                    isStarted = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
                hideProgress();
            }
        }
        mIsPlaying = true;
        AudiostreamMetadataManager.getInstance()
                .setUri(mUri)
                .setOnNewMetadataListener(this)
                .setUserAgent(UserAgent.WINDOWS_MEDIA_PLAYER)
                .start();
    }

    @Override
    public void play(String stringUri) {
        play(Uri.parse(stringUri));
    }

    @Override
    public void play() {
        play(mUri);

    }

    @Override
    public void stop() {
        Log.e(LOG_TAG, "-----stop------");
        if (mIsPlaying) {
            mPlayer.reset();
            mIsPlaying = false;
            AudiostreamMetadataManager.getInstance().stop();
            hideProgress();
        }
    }

    @Override
    public void pause() {
        Log.e(LOG_TAG, "-----pause------");
        if (mIsPlaying) {
            mPlayer.pause();
            isStarted = false;
            mIsPlaying = false;
            AudiostreamMetadataManager.getInstance().stop();
            hideProgress();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        isPrepared = true;
        isError = false;
        if (isStarted) {
            mp.start();
        }

        long end = System.currentTimeMillis() - start;
        Log.e(LOG_TAG, "-----onPrepared  ------ :  " + end);

        hideProgress();
        mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {

                long end = System.currentTimeMillis() - start;
                Log.e(LOG_TAG, "-----onBufferingUpdate  ------ :  " + end);

                if (percent == 100) {
                    long end2 = System.currentTimeMillis() - start;
                    Log.e(LOG_TAG, "-----onBufferingUpdate  percent ------ :  " + end2);
                    hideProgress();
                }
            }
        });
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(LOG_TAG, "onError");
        mp.reset();
     //   mUri = null;
        isError = true;
        isPrepared = false;
        isStarted = false;
        errorOnPlay();
        Log.e(LOG_TAG, "end onError");
        return false;
    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.e(LOG_TAG, "onCompletion");
        checkInternetConn();
    }

    @Override
    public void onNewHeaders(String stringUri, List<String> name, List<String> desc, List<String> br, List<String> genre, List<String> info) {
        StringBuilder sName = new StringBuilder();
        for (String item : name) {
            sName.append(item).append(" ");
        }

        StringBuilder sDesc = new StringBuilder();
        for (String item : desc) {
            sDesc.append(item).append(" ");
        }

        StringBuilder sBr = new StringBuilder();
        for (String item : br) {
            sBr.append(item).append(" ");
        }

        StringBuilder sGenre = new StringBuilder();
        for (String item : genre) {
            sGenre.append(item).append(" ");
        }

        StringBuilder sInfo = new StringBuilder();
        for (String item : info) {
            sInfo.append(item).append(" ");
        }

        Log.v(LOG_TAG, String.format("onNewHeaders: Uri - %s", stringUri));
        Log.v(LOG_TAG, String.format("Icy-name: %s", sName.toString()));
        Log.v(LOG_TAG, String.format("Icy-description: %s", sDesc.toString()));
        Log.v(LOG_TAG, String.format("Icy-br: %s", sBr.toString()));
        Log.v(LOG_TAG, String.format("Icy-genre: %s", sGenre.toString()));
        Log.v(LOG_TAG, String.format("Ice-audio-info: %s", sInfo.toString()));

        Intent intent = new Intent(BroadcastAction.HEADERS);
        intent.putExtra(BundleArg.HEADER_NAME, sName.toString());
        intent.putExtra(BundleArg.HEADER_DESCRIPTION, sDesc.toString());
        intent.putExtra(BundleArg.HEADER_BR, sBr.toString());
        intent.putExtra(BundleArg.HEADER_GENRE, sGenre.toString());
        intent.putExtra(BundleArg.HEADER_INFO, sInfo.toString());
        sendBroadcast(intent);
    }

    @Override
    public void onNewStreamTitle(String stringUri, String streamTitle) {
        Log.v(LOG_TAG, String.format("Uri: %1$s # streamTitle: %2$s", stringUri, streamTitle));
        Intent intent = new Intent(BroadcastAction.STREAM_TITLE);
        intent.putExtra(BundleArg.STREAM_TITLE, streamTitle);
        sendBroadcast(intent);
    }


    private void hideProgress() {
        Intent intent = new Intent(BroadcastAction.PROGRESS);
        intent.putExtra(Keys.PROGRESS_STATUS, Keys.HIDE);
        sendBroadcast(intent);
    }

    private void showProgress() {
        Intent intent = new Intent(BroadcastAction.PROGRESS);
        intent.putExtra(Keys.PROGRESS_STATUS, Keys.SHOW);
        sendBroadcast(intent);
    }

    private void errorOnPlay() {
        Intent intent = new Intent(BroadcastAction.PROGRESS);
        intent.putExtra(Keys.PROGRESS_STATUS, Keys.ERROR);
        sendBroadcast(intent);
    }

    private void checkInternetConn() {
        Intent intent = new Intent(BroadcastAction.PROGRESS);
        intent.putExtra(Keys.PROGRESS_STATUS, Keys.CHECK_INTERNET);
        sendBroadcast(intent);
    }

}
