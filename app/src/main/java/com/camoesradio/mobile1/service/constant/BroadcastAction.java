package com.camoesradio.mobile1.service.constant;


public class BroadcastAction
{
    public static final String STREAM_TITLE = "action_stream_title";
    public static final String HEADERS = "action_headers";
    public static final String PROGRESS = "action_progress";

}
