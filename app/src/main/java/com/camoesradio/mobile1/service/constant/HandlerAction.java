package com.camoesradio.mobile1.service.constant;

public class HandlerAction
{
    public static final int PLAY = 0;
    public static final int PLAY_STRING_URI = 1;
    public static final int PLAY_URI = 2;
    public static final int STOP = 3;
    public static final int PAUSE=4;
}
