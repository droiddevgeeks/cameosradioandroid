package com.camoesradio.mobile1.service.util;

/**
 * Created by Pramod on 31/1/18.
 */

public interface ServiceCallbacks {
    public void showProgress();
    public void hideProgress();
}
