package com.camoesradio.mobile1.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;


/*import com.camoesradio.mobile1.contest.ContestActivity;*/
import com.camoesradio.mobile1.main.MainActivity;
import com.camoesradio.mobile1.R;

import java.util.Timer;
import java.util.TimerTask;



public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        //startMainActivity();
        checkIfFirstTime();
    }

    // checks to see if user has opened the app for the first time
    private void checkIfFirstTime() {
        boolean isFirstRun = getSharedPreferences("com.camoesradio.mobile1", MODE_PRIVATE).getBoolean("isFirstTime", true);

            startMainActivity();

    }

    //method to start startMainActivity
    private void startMainActivity() {
        TimerTask timerTask=new TimerTask() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        };
        Timer timer=new Timer();
        timer.schedule(timerTask, 1000);
    }

/*    private void startContestActivity() {
        TimerTask timerTask=new TimerTask() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this,ContestActivity.class);
                startActivity(intent);
                finish();
            }
        };
        Timer timer=new Timer();
        timer.schedule(timerTask, 1000);
    }*/
}
