package com.camoesradio.mobile1.utils;

/**
 * Created by sumit on 31/3/17.
 */

public class AppConstants {

    public static final String FB_URL="https://www.facebook.com/camoesradio/";
    public static final String TWITTER_URL="https://twitter.com/search?q=camoes%20radio%5C&src=typd";
    public static final String YOUTUBE_URL="https://www.youtube.com/channel/UCttMZuJcFXsL5tl7cYjwFzw";
    public static final String SCHEDULE_URL="https://camoesradio.com/weekly-schedule/";
    public static final String OUR_SHOWS_URL="https://camoesradio.com/shows/";
    public static final String OUR_BLOG_URL="https://camoesradio.com/blog/";
    public static final String CAMOES_TV_URL="https://camoesradio.com/tv";
    public static final String ABOUT_US_URL="https://camoesradio.com/about.page";
    public static final String CONTACT_US_EMAIL_ID="info@camoesradio.com";
    public static final String TOP_BANNER="https://camoesradio.com/camoes-radio-chha-1610-am.show";
    public static final String BOTTOM_BANNER="https://camoesradio.com/balanca-toronto.show";

    // contest event
    public static final String CONTEST_URL="http://indigital.company/contest";
}
