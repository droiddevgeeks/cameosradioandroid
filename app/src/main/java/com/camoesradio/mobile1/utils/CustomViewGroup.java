package com.camoesradio.mobile1.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by sumit on 22/3/17.
 */

public class CustomViewGroup extends ViewGroup {


    public CustomViewGroup(Context context) {
        super(context);
    }

    public CustomViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
        /*BG Image*/
        int heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.85), MeasureSpec.EXACTLY);
        int widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth()), MeasureSpec.EXACTLY);
        measureChild(getChildAt(0), widthSpec, heightSpec);

        /*Play Pause Image*/
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.30), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.30), MeasureSpec.EXACTLY);
        measureChild(getChildAt(1), widthSpec, heightSpec);

        /*Next Button View*/
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.07), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.07), MeasureSpec.EXACTLY);
        measureChild(getChildAt(2), widthSpec, heightSpec);

        /*Previous button view*/
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.07), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.07), MeasureSpec.EXACTLY);
        measureChild(getChildAt(3), widthSpec, heightSpec);

        /*Cloud Image View*/
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.16), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.12), MeasureSpec.EXACTLY);
        measureChild(getChildAt(4), widthSpec, heightSpec);

/*
        */
/*song header View*//*

        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.20), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.40), MeasureSpec.EXACTLY);
        measureChild(getChildAt(5), widthSpec, heightSpec);
*/

         /*song name textView*/
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.06), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth()), MeasureSpec.EXACTLY);
        measureChild(getChildAt(5), widthSpec, heightSpec);

     /*   *//*faccbook image view*//*
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        measureChild(getChildAt(7), widthSpec, heightSpec);

        *//*twitter image view*//*
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        measureChild(getChildAt(8), widthSpec, heightSpec);

        *//*google image view*//*
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        measureChild(getChildAt(9), widthSpec, heightSpec);

        *//*Menu image view*//*
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.10), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.10), MeasureSpec.EXACTLY);
        measureChild(getChildAt(10), widthSpec, heightSpec);

           /*song image view*/
        heightSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredHeight() * 0.90), MeasureSpec.EXACTLY);
        widthSpec = MeasureSpec.makeMeasureSpec((int) (getMeasuredWidth() * 0.90 ), MeasureSpec.EXACTLY);
        measureChild(getChildAt(6), widthSpec, heightSpec);


    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        /*BG Image Layout*/
        View bgView = getChildAt(0);
        bgView.layout(0, 0, getMeasuredWidth(), bgView.getMeasuredHeight());

        /*Play Pause Image layout*/
        View playView = getChildAt(1);
        playView.layout((getMeasuredWidth() >> 1) - (playView.getMeasuredWidth() >> 1), (bgView.getMeasuredHeight()) - (playView.getMeasuredHeight() >> 1), ((getMeasuredWidth() >> 1) - (playView.getMeasuredWidth() >> 1)) + playView.getMeasuredWidth(), bgView.getMeasuredHeight() - (playView.getMeasuredHeight() >> 1) + playView.getMeasuredHeight());

        /*Next Button View layout*/
        View nextView = getChildAt(2);
        nextView.layout((getMeasuredWidth() >> 1) + ((getMeasuredWidth() >> 1) >> 1), bgView.getMeasuredHeight(), ((getMeasuredWidth() >> 1) + ((getMeasuredWidth() >> 1) >> 1)) + nextView.getMeasuredWidth(), bgView.getMeasuredHeight() + nextView.getMeasuredHeight());

        /*Previous button view layout*/
        View prevView = getChildAt(3);
        prevView.layout(((getMeasuredWidth() >> 1) - ((getMeasuredWidth() >> 1) >> 1)) - prevView.getMeasuredWidth(), bgView.getMeasuredHeight(), (((getMeasuredWidth() >> 1) - ((getMeasuredWidth() >> 1) >> 1)) - prevView.getMeasuredWidth()) + prevView.getMeasuredWidth(), bgView.getMeasuredHeight() + prevView.getMeasuredHeight());

        /*Cloud Image View layout*/
        View cloudView = getChildAt(4);
        cloudView.layout((getMeasuredWidth() >> 1) - (cloudView.getMeasuredWidth() >> 1), (bgView.getMeasuredHeight()) - (int) (playView.getMeasuredHeight() * 0.8), ((getMeasuredWidth() >> 1) - (cloudView.getMeasuredWidth() >> 1)) + cloudView.getMeasuredWidth(), bgView.getMeasuredHeight() - (int) (playView.getMeasuredHeight() * 0.8) + cloudView.getMeasuredHeight());


          /*song name textView*/
        View textView = getChildAt(5);
        textView.layout((getMeasuredWidth() >> 1) - (textView.getMeasuredWidth() >> 1), (bgView.getMeasuredHeight()) - ((int) (playView.getMeasuredHeight() * 1.2)), ((getMeasuredWidth() >> 1) - (textView.getMeasuredWidth() >> 1)) + textView.getMeasuredWidth(), bgView.getMeasuredHeight() - ((int) (playView.getMeasuredHeight() * 1.2)) + textView.getMeasuredHeight());
        int widthPadding = textView.getMeasuredWidth()>>1;
        int heightPadding = textView.getMeasuredHeight()>>1;
     /*   *//*Facebook image view*//*
        View fbView = getChildAt(7);
        int widthPadding = fbView.getMeasuredWidth()>>1;
        int heightPadding = fbView.getMeasuredHeight()>>1;
        int twitter_btn_height_padding= (int) (fbView.getMeasuredHeight()*0.4);
        fbView.layout(widthPadding, heightPadding,widthPadding+ fbView.getMeasuredWidth(), heightPadding+fbView.getMeasuredHeight());

        *//*twitter image view*//*
        View twitterView = getChildAt(8);
        twitterView.layout(widthPadding, (twitter_btn_height_padding+heightPadding)+fbView.getMeasuredHeight(),+widthPadding + twitterView.getMeasuredWidth(), (twitter_btn_height_padding+heightPadding)+fbView.getMeasuredHeight() + twitterView.getMeasuredHeight());

        *//*google plus image view*//*
        View googleView = getChildAt(9);
        googleView.layout(widthPadding, ((twitter_btn_height_padding*2)+heightPadding)+fbView.getMeasuredHeight() + twitterView.getMeasuredHeight(), widthPadding + googleView.getMeasuredWidth(), ((twitter_btn_height_padding*2)+heightPadding)+fbView.getMeasuredHeight() + twitterView.getMeasuredHeight() + googleView.getMeasuredHeight());

        *//*Menu image view*//*
        View nevView = getChildAt(10);
        nevView.layout(getMeasuredWidth() - (nevView.getMeasuredWidth()+(nevView.getMeasuredWidth()>>1)), heightPadding,(getMeasuredWidth() - (nevView.getMeasuredWidth()+(nevView.getMeasuredWidth()>>1)))+nevView.getMeasuredWidth(),heightPadding+nevView.getMeasuredHeight());

           *//*header View*//*
        View headerImageView = getChildAt(5);
        headerImageView.layout((getMeasuredWidth() >> 1) - (headerImageView.getMeasuredWidth() >> 1), heightPadding, ((getMeasuredWidth() >> 1) - (headerImageView.getMeasuredWidth() >> 1)) + headerImageView.getMeasuredWidth(), heightPadding+headerImageView.getMeasuredHeight());
*/
     /*song image View*/
        int w= (int)(getMeasuredWidth()*0.60);
        int h= (int)(getMeasuredWidth()*0.40);
        View songImageView = getChildAt(6);
        songImageView.layout((getMeasuredWidth() >> 1) - (w>>1), textView.getMeasuredHeight(), ((getMeasuredWidth() >> 1) - (w>>1)) + w, (textView.getMeasuredHeight())+h);

    }
}
