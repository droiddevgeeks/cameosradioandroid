package com.camoesradio.mobile1.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by sumit on 22/3/17.
 */

public class HelperMethods {

    /**
    * method is to check internet connection
    * @param mContext context of the activity
    * */

    public static boolean isConnectingToInternet(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                //noinspection deprecation
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Log.d("Network", "NETWORKNAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        //Toast.makeText(mContext, "Your are not connected to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    /**
     * show short length toast
     */
    public static void showToastS(Context context, String message) {
        if (context != null) {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            //toast.setGravity(Gravity.BOTTOM, 0,0);
            toast.show();
        }
    }


    /**
     * show long length toast
     */
    public static void showToastL(Context context, String message) {
        if (context != null) {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            //toast.setGravity(Gravity.BOTTOM|Gravity.CENTER, 0,0);
            toast.show();
        }
    }

        public static Typeface getTypeFaceOpenSans(Context context) {
            return Typeface.createFromAsset(context.getAssets(), "Myriad Pro Regular.ttf");
        }


}
