package com.camoesradio.mobile1.utils;

/**
 * Created by Pramod on 31/1/18.
 */

public class Keys {
    public static final String HIDE="hide";
    public static final String SHOW="show";
    public static final String ERROR="error";
    public static final String CHECK_INTERNET="check_internet";
    public static final String PROGRESS_STATUS="progress_status";
}
