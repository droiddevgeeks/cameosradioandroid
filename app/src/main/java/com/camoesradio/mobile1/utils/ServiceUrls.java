package com.camoesradio.mobile1.utils;

/**
 * Created by sumit on 21/3/17.
 */

public class ServiceUrls {


   /* CamoesRadio's metadata API can be accessed here:

    http://camoesradio.airtime.pro/api/live-info

    Live IceCast stream is here:

    http://camoesradio.out.airtime.pro:8000/camoesradio_a

    http://camoesradio.out.airtime.pro:8000/camoesradio_b*/

    public static final String BASE_URL ="camoesradio.airtime";
    public static final String META_DATA =".pro/api/live-info";
    public static String LIVE_STREAM ="http://camoesradio.out.airtime.pro:8000/camoesradio_a";
    public static String NEXT_SONG ="http://camoesradio.out.airtime.pro:8000/camoesradio_b";



}
